package com.countryinfo.android;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;

import com.countryinfo.android.fragment.CountryDetailFragment;
import com.countryinfo.android.fragment.CountryListFragment;
import com.countryinfo.android.jsonmodel.Country;
import com.countryinfo.android.network.CountryService;

public class MainActivity extends FragmentActivity {
	
	private ArrayList<Country> countries;
	public CountryListFragment listFragment;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		listFragment = new CountryListFragment();
		new CountryService().getApiService().getAllCountries(new Callback<ArrayList<Country>>() {

			@Override
			public void failure(RetrofitError error) {
				//TODO ERROR DIALOG?
			}

			@Override
			public void success(ArrayList<Country> countries, Response response) {
				MainActivity.this.countries = countries;
				getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment_container, new CountryListFragment()).commit();
			}
			
		});
	}
	
	@Override
	public void onBackPressed() {
		if(getSupportFragmentManager().getFragments().get(0) instanceof CountryDetailFragment) {
			getSupportFragmentManager().beginTransaction()
			.replace(R.id.fragment_container, 
					new CountryListFragment()).commit();
			return;
		} 
		
		super.onBackPressed();
	}
	
	
	public ArrayList<Country> getCountries() {
		return countries;
	}
}
