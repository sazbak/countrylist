package com.countryinfo.android.adapter;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.countryinfo.android.R;
import com.countryinfo.android.jsonmodel.Country;
import com.squareup.picasso.Picasso;

public class CountryListAdapter extends ArrayAdapter<Country> {

	private static final String PLACEHOLDER = "placeholder";
	private static final String BASE_URL = String.format("http://www.geonames.org/flags/x/%s.gif", PLACEHOLDER) ;
	private static final int FLAG_WIDTH = 50;
	private static final int FLAG_HEIGHT = 50;

	private Context context;
	private List<Country> countries;
	private LayoutInflater inflater;

	public CountryListAdapter(Context context, int resource,
			List<Country> objects) {
		super(context, resource, objects);
		inflater = (LayoutInflater) context
				.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		this.context = context;
		this.countries = objects;
	}

	static class ViewHolder {
		TextView name;
		TextView region;
		ImageView flag;
		TextView population;
	}
	
	public Country	getItem(int position) {
		return countries.get(position);
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;

		if (convertView == null) {
			convertView = inflater.inflate(R.layout.rowlayout, null);
			holder = new ViewHolder();
			holder.name = (TextView) convertView.findViewById(R.id.countryname);
			holder.region = (TextView) convertView.findViewById(R.id.region);
			holder.flag = (ImageView) convertView.findViewById(R.id.flag);
			holder.population = (TextView) convertView.findViewById(R.id.population);
			convertView.setTag(holder);
		} else
			holder = (ViewHolder) convertView.getTag();

		holder.name.setText(countries.get(position).getName());
		holder.region.setText(countries.get(position).getRegion());
		Picasso.with(context).load(
				BASE_URL.replace(PLACEHOLDER, countries.get(position).getAlpha2Code().toLowerCase()))
				.resize(FLAG_WIDTH, FLAG_HEIGHT).into(holder.flag);
		holder.population.setText(String.valueOf(countries.get(position).getPopulation()));

		return convertView;
	}
}
