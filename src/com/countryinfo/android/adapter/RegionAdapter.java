package com.countryinfo.android.adapter;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.countryinfo.android.R;
import com.countryinfo.android.jsonmodel.Country;

public class RegionAdapter extends ArrayAdapter {
	
	private Context context;
	private List<Country> countries;
	private LayoutInflater inflater;
	

	public RegionAdapter(Context context, int resource, List<Country> objects) {
		super(context, resource, objects);
		
		inflater = (LayoutInflater) context
				.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		this.context = context;
		this.countries = objects;
	}
	
	static class ViewHolder {
		TextView name;
	}
	
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;

		if (convertView == null) {
			convertView = inflater.inflate(R.layout.sub_countries, null);
			holder = new ViewHolder();
			holder.name = (TextView) convertView.findViewById(R.id.subcountryname);
			convertView.setTag(holder);
		} else
			holder = (ViewHolder) convertView.getTag();

		holder.name.setText(countries.get(position).getName());

		return convertView;
	}

}
