package com.countryinfo.android.jsonmodel;

import java.util.ArrayList;
import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;

public class Country implements Parcelable, Comparable<Country> {
	
	private String name;
	private String capital;
	private List<String> altSpellings = new ArrayList<String>();
	private String relevance;
	private String region;
	private String subregion;
	private Translations translations;
	private String population;
	private List<Float> latlng = new ArrayList<Float>();
	private String demonym;
	private Float area;
	private Float gini;
	private List<String> timezones = new ArrayList<String>();
	private List<String> borders = new ArrayList<String>();
	private String nativeName;
	private List<String> callingCodes = new ArrayList<String>();
	private List<String> topLevelDomain = new ArrayList<String>();
	private String alpha2Code;
	private String alpha3Code;
	private List<String> currencies = new ArrayList<String>();
	private List<String> languages = new ArrayList<String>();
	
	@Override
	public int compareTo(Country another) {
		return translations.compareTo(another.getTranslations());
	}
	

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCapital() {
		return capital;
	}
	public void setCapital(String capital) {
		this.capital = capital;
	}
	public List<String> getAltSpellings() {
		return altSpellings;
	}
	public void setAltSpellings(List<String> altSpellings) {
		this.altSpellings = altSpellings;
	}
	public String getRelevance() {
		return relevance;
	}
	public void setRelevance(String relevance) {
		this.relevance = relevance;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getSubregion() {
		return subregion;
	}
	public void setSubregion(String subregion) {
		this.subregion = subregion;
	}
	public Translations getTranslations() {
		return translations;
	}
	public void setTranslations(Translations translations) {
		this.translations = translations;
	}
	public String getPopulation() {
		return population;
	}
	public void setPopulation(String population) {
		this.population = population;
	}
	public List<Float> getLatlng() {
		return latlng;
	}
	public void setLatlng(List<Float> latlng) {
		this.latlng = latlng;
	}
	public String getDemonym() {
		return demonym;
	}
	public void setDemonym(String demonym) {
		this.demonym = demonym;
	}
	public Float getArea() {
		return area;
	}
	public void setArea(Float area) {
		this.area = area;
	}
	public Float getGini() {
		return gini;
	}
	public void setGini(Float gini) {
		this.gini = gini;
	}
	public List<String> getTimezones() {
		return timezones;
	}
	public void setTimezones(List<String> timezones) {
		this.timezones = timezones;
	}
	public List<String> getBorders() {
		return borders;
	}
	public void setBorders(List<String> borders) {
		this.borders = borders;
	}
	public String getNativeName() {
		return nativeName;
	}
	public void setNativeName(String nativeName) {
		this.nativeName = nativeName;
	}
	public List<String> getCallingCodes() {
		return callingCodes;
	}
	public void setCallingCodes(List<String> callingCodes) {
		this.callingCodes = callingCodes;
	}
	public List<String> getTopLevelDomain() {
		return topLevelDomain;
	}
	public void setTopLevelDomain(List<String> topLevelDomain) {
		this.topLevelDomain = topLevelDomain;
	}
	public String getAlpha2Code() {
		return alpha2Code;
	}
	public void setAlpha2Code(String alpha2Code) {
		this.alpha2Code = alpha2Code;
	}
	public String getAlpha3Code() {
		return alpha3Code;
	}
	public void setAlpha3Code(String alpha3Code) {
		this.alpha3Code = alpha3Code;
	}
	public List<String> getCurrencies() {
		return currencies;
	}
	public void setCurrencies(List<String> currencies) {
		this.currencies = currencies;
	}
	public List<String> getLanguages() {
		return languages;
	}
	public void setLanguages(List<String> languages) {
		this.languages = languages;
	}

    protected Country(Parcel in) {
        name = in.readString();
        capital = in.readString();
        if (in.readByte() == 0x01) {
            altSpellings = new ArrayList<String>();
            in.readList(altSpellings, String.class.getClassLoader());
        } else {
            altSpellings = null;
        }
        relevance = in.readString();
        region = in.readString();
        subregion = in.readString();
        translations = (Translations) in.readValue(Translations.class.getClassLoader());
        population = in.readString();
        if (in.readByte() == 0x01) {
            latlng = new ArrayList<Float>();
            in.readList(latlng, Float.class.getClassLoader());
        } else {
            latlng = null;
        }
        demonym = in.readString();
        area = in.readByte() == 0x00 ? null : in.readFloat();
        gini = in.readByte() == 0x00 ? null : in.readFloat();
        if (in.readByte() == 0x01) {
            timezones = new ArrayList<String>();
            in.readList(timezones, String.class.getClassLoader());
        } else {
            timezones = null;
        }
        if (in.readByte() == 0x01) {
            borders = new ArrayList<String>();
            in.readList(borders, String.class.getClassLoader());
        } else {
            borders = null;
        }
        nativeName = in.readString();
        if (in.readByte() == 0x01) {
            callingCodes = new ArrayList<String>();
            in.readList(callingCodes, String.class.getClassLoader());
        } else {
            callingCodes = null;
        }
        if (in.readByte() == 0x01) {
            topLevelDomain = new ArrayList<String>();
            in.readList(topLevelDomain, String.class.getClassLoader());
        } else {
            topLevelDomain = null;
        }
        alpha2Code = in.readString();
        alpha3Code = in.readString();
        if (in.readByte() == 0x01) {
            currencies = new ArrayList<String>();
            in.readList(currencies, String.class.getClassLoader());
        } else {
            currencies = null;
        }
        if (in.readByte() == 0x01) {
            languages = new ArrayList<String>();
            in.readList(languages, String.class.getClassLoader());
        } else {
            languages = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(capital);
        if (altSpellings == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(altSpellings);
        }
        dest.writeString(relevance);
        dest.writeString(region);
        dest.writeString(subregion);
        dest.writeValue(translations);
        dest.writeValue(population);
        if (latlng == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(latlng);
        }
        dest.writeString(demonym);
        if (area == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeFloat(area);
        }
        if (gini == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeFloat(gini);
        }
        if (timezones == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(timezones);
        }
        if (borders == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(borders);
        }
        dest.writeString(nativeName);
        if (callingCodes == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(callingCodes);
        }
        if (topLevelDomain == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(topLevelDomain);
        }
        dest.writeString(alpha2Code);
        dest.writeString(alpha3Code);
        if (currencies == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(currencies);
        }
        if (languages == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(languages);
        }
    }

    public static final Parcelable.Creator<Country> CREATOR = new Parcelable.Creator<Country>() {
        @Override
        public Country createFromParcel(Parcel in) {
            return new Country(in);
        }

        @Override
        public Country[] newArray(int size) {
            return new Country[size];
        }
    };
}