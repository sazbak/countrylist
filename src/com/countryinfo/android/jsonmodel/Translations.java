package com.countryinfo.android.jsonmodel;

import com.countryinfo.android.application.CountryListApplication;

import android.os.Parcel;
import android.os.Parcelable;

public class Translations implements Parcelable, Comparable<Translations> {
	
	private static final int PRESERVE_ORDER = 1;

	public enum NativeLanguage {
		GERMAN("de"),
		FRENCH("fr"),
		ITALIAN("it"),
		SPANISH("es"),
		JAPANESE("ja");

		private String countryCode;

		NativeLanguage(String countryCode) {
			this.countryCode = countryCode;
		}
		
		public String getCountryCode() {
			return countryCode;
		}
	}

	private String de;
	private String es;
	private String fr;
	private String ja;
	private String it;

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(de);
		dest.writeString(es);
		dest.writeString(fr);
		dest.writeString(ja);
		dest.writeString(it);
	}

	public static final Parcelable.Creator<Translations> CREATOR = new Parcelable.Creator<Translations>() {
		@Override
		public Translations createFromParcel(Parcel in) {
			return new Translations(in);
		}

		@Override
		public Translations[] newArray(int size) {
			return new Translations[size];
		}
	};

	protected Translations(Parcel in) {
		de = in.readString();
		es = in.readString();
		fr = in.readString();
		ja = in.readString();
		it = in.readString();
	}

	@Override
	public int compareTo(Translations another) {

		String locale = CountryListApplication.defSystemLanguage;

		if(locale.equals(NativeLanguage.SPANISH.getCountryCode())) {
			if(es != null && another.es != null) {
				return es.compareTo(another.es);
			}	
		} else if(locale.equals(NativeLanguage.JAPANESE.getCountryCode())) {
			if(ja != null && another.ja != null) {
				return ja.compareTo(another.ja);
			}
		} else if(locale.equals(NativeLanguage.FRENCH.getCountryCode())) {
			if(fr != null && another.fr != null) {
				return fr.compareTo(another.fr);
			}
		} else if(locale.equals(NativeLanguage.GERMAN.getCountryCode())) {
			if(de != null && another.de != null) {
				return de.compareTo(another.de);
			}	
		} else if(locale.equals(NativeLanguage.ITALIAN.getCountryCode())) {
			if(it != null && another.it != null) {
				return it.compareTo(another.it);
			}	
		}

		return PRESERVE_ORDER;
	}

	public String getDe() {
		return de;
	}
	public void setDe(String de) {
		this.de = de;
	}
	public String getEs() {
		return es;
	}
	public void setEs(String es) {
		this.es = es;
	}
	public String getFr() {
		return fr;
	}
	public void setFr(String fr) {
		this.fr = fr;
	}
	public String getJa() {
		return ja;
	}
	public void setJa(String ja) {
		this.ja = ja;
	}
	public String getIt() {
		return it;
	}
	public void setIt(String it) {
		this.it = it;
	}
}