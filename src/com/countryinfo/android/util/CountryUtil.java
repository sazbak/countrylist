package com.countryinfo.android.util;

import java.util.ArrayList;

import com.countryinfo.android.jsonmodel.Country;

public class CountryUtil {
	
	public ArrayList<Country> getCountriesWithSameRegion(Country country, ArrayList<Country> allCountries) {
		String region = country.getRegion();
		ArrayList<Country> sameRegionCountries = new ArrayList<Country>();

		for(Country actualCountry : allCountries) {
			if(actualCountry.getRegion().equals(region)) {
				sameRegionCountries.add(actualCountry);
			}
		}

		return sameRegionCountries;
	}
	
	public ArrayList<Country> getBorderingCountries(Country country, ArrayList<Country> allCountries) {
		ArrayList<Country> borderingCountries = new ArrayList<Country>();

		for(Country actualCountry : allCountries) {
			for(String countryCode : actualCountry.getBorders()) {
				if(countryCode.equals(country.getAlpha3Code())) {
					borderingCountries.add(actualCountry);
				}
			}
		}

		return borderingCountries;
	}

}
