package com.countryinfo.android.network;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;

import com.countryinfo.android.jsonmodel.Country;

public class CountryService {

	public static final String BASE_URL = "http://restcountries.eu/rest/v1";

	private CountryServiceEndpointInterface apiService;

	public CountryService() {
		RestAdapter restAdapter = new RestAdapter.Builder()
		.setEndpoint(BASE_URL)
		.build();
		apiService = restAdapter.create(CountryServiceEndpointInterface.class);
	}

	public interface CountryServiceEndpointInterface {

		@GET("/all")
		void getAllCountries(Callback<ArrayList<Country>> countries);

	}
	
	public CountryServiceEndpointInterface getApiService() {
		return apiService;
	}

}
