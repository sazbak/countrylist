package com.countryinfo.android.fragment;


import java.util.ArrayList;
import java.util.Collections;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.countryinfo.android.MainActivity;
import com.countryinfo.android.R;
import com.countryinfo.android.adapter.CountryListAdapter;
import com.countryinfo.android.jsonmodel.Country;

public class CountryListFragment extends ListFragment {

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		final ArrayList<Country> countries = ((MainActivity)getActivity()).getCountries();
		Collections.sort(countries);
		CountryListAdapter adapter = new CountryListAdapter(getActivity(), 0, countries);
		setListAdapter(adapter);
		getListView().setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Country selectedCountry =  countries.get(position);
				getActivity().getSupportFragmentManager().beginTransaction()
				.replace(R.id.fragment_container, 
						CountryDetailFragment.newInstance(
								selectedCountry)).commit();
			}
		});
	}
}
