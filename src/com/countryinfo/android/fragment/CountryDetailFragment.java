package com.countryinfo.android.fragment;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.countryinfo.android.MainActivity;
import com.countryinfo.android.R;
import com.countryinfo.android.adapter.RegionAdapter;
import com.countryinfo.android.jsonmodel.Country;
import com.countryinfo.android.util.CountryUtil;
import com.countryinfo.android.view.CountryDetailView;

public class CountryDetailFragment extends Fragment {

	private static final String SELECTED_COUNTRY = "selected_country";

	private ArrayList<Country> sameRegionCountries;
	private ArrayList<Country> borderingCountries;
	private CountryDetailView countryDetailView;
	private Country country;

	public static CountryDetailFragment newInstance(Country selectedCountry) {
		CountryDetailFragment listFragment = new CountryDetailFragment();
		Bundle bundle = new Bundle();
		bundle.putParcelable(SELECTED_COUNTRY, selectedCountry);
		listFragment.setArguments(bundle);
		return listFragment;
	}

	@Override
	public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		country = getArguments().getParcelable(SELECTED_COUNTRY);
		ArrayList<Country> countries = ((MainActivity)getActivity()).getCountries();
		sameRegionCountries = new CountryUtil().getCountriesWithSameRegion(country, countries);
		borderingCountries = new CountryUtil().getBorderingCountries(country, countries);
		View view = inflater.inflate(R.layout.detailedview, container, false);
		setupViews(view);
		return view;
	}

	private void setupViews(View view) {
		countryDetailView = new CountryDetailView(view);
		countryDetailView.getCountryName().setText(country.getName());
		countryDetailView.getNativeCountryName().setText(country.getNativeName());
		countryDetailView.getCapital().setText(country.getCapital());
		countryDetailView.getPopulation().setText(country.getPopulation());
		countryDetailView.getBordersLabel().setText(view.getResources().getString(R.string.borders_label));
		countryDetailView.getRegionLabel().setText(view.getResources().getString(R.string.region_label));
		countryDetailView.getRegionCountries().setAdapter(
				new RegionAdapter(getActivity(), android.R.layout.simple_list_item_1, sameRegionCountries));
		countryDetailView.getBorderCountries().setAdapter(
				new RegionAdapter(getActivity(), android.R.layout.simple_list_item_1, borderingCountries));
		countryDetailView.getBorderCountries().setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				getActivity().getSupportFragmentManager().beginTransaction()
				.replace(R.id.fragment_container, 
						CountryDetailFragment.newInstance(
								borderingCountries.get(position))).commit();
			}
		});
	}

	

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

}
