package com.countryinfo.android.application;

import java.util.Locale;

import android.app.Application;
import android.content.res.Configuration;

public class CountryListApplication extends Application {

    public static String defSystemLanguage;

    @Override
    public void onCreate() {
        super.onCreate();

        defSystemLanguage = Locale.getDefault().getLanguage();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        defSystemLanguage = newConfig.locale.getLanguage();
    }

}
