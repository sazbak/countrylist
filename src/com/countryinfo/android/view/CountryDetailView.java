package com.countryinfo.android.view;

import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.countryinfo.android.R;

public class CountryDetailView {
	
	private final TextView countryName;
	private final TextView nativeCountryName;
	private final TextView capital;
	private final TextView regionLabel;
	private final TextView bordersLabel;
	private final TextView population;
	private final ListView regionCountries;
	private final ListView borderCountries;


	public CountryDetailView(View view) {
		countryName = (TextView) view.findViewById(R.id.countryname);
		nativeCountryName = (TextView) view.findViewById(R.id.native_countryname);
		capital = (TextView) view.findViewById(R.id.capital);
		regionLabel = (TextView) view.findViewById(R.id.region_label);
		bordersLabel = (TextView) view.findViewById(R.id.borders_label);
		population = (TextView) view.findViewById(R.id.population);
		regionCountries = (ListView) view.findViewById(R.id.region_country_list);
		borderCountries = (ListView) view.findViewById(R.id.borders);
	}

	public TextView getCountryName() {
		return countryName;
	}


	public TextView getNativeCountryName() {
		return nativeCountryName;
	}


	public TextView getCapital() {
		return capital;
	}


	public TextView getRegionLabel() {
		return regionLabel;
	}


	public TextView getBordersLabel() {
		return bordersLabel;
	}


	public TextView getPopulation() {
		return population;
	}


	public ListView getRegionCountries() {
		return regionCountries;
	}


	public ListView getBorderCountries() {
		return borderCountries;
	}
}
